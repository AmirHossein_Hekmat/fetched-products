import { mount } from "@vue/test-utils";
import search_filter from "../components/search_filter.vue";

describe("search_filter.vue", () => {
  it("this test checks the initial value of searchedProduct", () => {
    const wrapper = mount(search_filter, {
      data() {
        return {
          searchedProduct: "",
        };
      },
    });
    expect(searchedProduct).not.toBeNull();
  });
});
